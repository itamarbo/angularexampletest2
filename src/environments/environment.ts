// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCOQgVufIv6PvAPNdsIezPAVgtJM_eEcXI",
    authDomain: "angularexampletest2.firebaseapp.com",
    databaseURL: "https://angularexampletest2.firebaseio.com",
    projectId: "angularexampletest2",
    storageBucket: "angularexampletest2.appspot.com",
    messagingSenderId: "899097564809",
    appId: "1:899097564809:web:b6f3492a59cfa1a5211858",
    measurementId: "G-X03H68K9L2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
