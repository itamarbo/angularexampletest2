import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user:Observable<User | null>;

  constructor(public afAuth:AngularFireAuth, private router:Router) {
    this.user = this.afAuth.authState;
   }

   signup(email:string, password:string){
    this.afAuth
    .auth.createUserWithEmailAndPassword(email,password).then
    (res =>{ console.log('Succesful Signup', res)})
    .catch(function(error) {
     // Handle Errors here.
      // var errorCode = error.code;
      var errorCode = error.code;
      var errorMessage = error.message;
      alert(errorMessage);
      console.log(error);
      console.log(errorCode);
      });
    }

   login(email:string, password:string){
     this.afAuth
     .auth.signInWithEmailAndPassword(email,password).then
    (res =>{ console.log('Succesful Login', res)})
    .catch(function(error) {
     // Handle Errors here.
      // var errorCode = error.code;
      var errorCode = error.code;
      var errorMessage = error.message;
      alert(errorMessage);
      console.log(error);
      console.log(errorCode);
      });
    }

   logout(){
     this.afAuth.auth.signOut();
     this.router.navigate(['/login']);
    }
}
