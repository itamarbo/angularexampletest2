import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password:string;
   
  constructor(public authservice:AuthService, private router: Router, private route:ActivatedRoute) { }
  
  ngOnInit() {
  
  }
  onSubmit(){
    this.authservice.login(this.email, this.password);
    this.router.navigate(['/successlogin']);
  }
}
