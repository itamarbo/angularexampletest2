import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class WorkersService {

  constructor(public httpClient:HttpClient, private db:AngularFirestore) { }

  getWorkers():Observable<any[]>{   //האפליקציה מושכת מידע מה database
    return this.db.collection('workers').valueChanges({idField:'id'});
  }

  addWorker(username:string, name:string, city:string){
    const worker = {username:username, name:name, city:city};
    this.db.collection('workers').add(worker);
  }

  deleteWorker(id:string){
    this.db.doc(`workers/${id}`).delete();
  }

  updateWorker(id:string, username:string, name:string, city:string){
    this.db.doc(`workers/${id}`).update({
      username:username,
      name:name,
      city:city
    })
  }

  getWorker(id:string):Observable<any>{
    return this.db.doc(`workers/${id}`).get()
  }
  
  
}
