import { WorkersService } from './../workers.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-workers',
  templateUrl: './workers.component.html',
  styleUrls: ['./workers.component.css']
})
export class WorkersComponent implements OnInit {

  workers:any=[];
  username:string;
  name:string;
  city:string;

  constructor(public workersService:WorkersService) { }

  ngOnInit() {
    this.workersService.getWorkers().subscribe(workers =>{
      console.log(workers);
      this.workers=workers;
    })
  }

  deleteWorker(id:string){
    this.workersService.deleteWorker(id);
  }
}
