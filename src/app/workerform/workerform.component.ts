import { Router, ActivatedRoute } from '@angular/router';
import { WorkersService } from './../workers.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-workerform',
  templateUrl: './workerform.component.html',
  styleUrls: ['./workerform.component.css']
})
export class WorkerformComponent implements OnInit {

  username:string;
  name:string;
  city:string;
  id:string;
  isEdit:boolean = false;
  buttonText:string = "Add Worker";

  constructor(private workersService:WorkersService, private router:Router, private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params.id;
    console.log(this.id);
    if(this.id){
      this.isEdit = true;
      this.buttonText = "Edit Worker";
      this.workersService.getWorker(this.id).subscribe(
        worker => {
          this.username = worker.data().username;
          this.name = worker.data().name;
          this.city = worker.data().city;
        }
      )
    }
  }

  onSubmit(){
    if(this.isEdit){
      this.workersService.updateWorker(this.id, this.username, this.name, this.city)
    }
    else{
      this.workersService.addWorker(this.username, this.name, this.city)
    }
    this.router.navigate(['/workers']);
  }


}
