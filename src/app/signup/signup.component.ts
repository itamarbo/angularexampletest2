import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  email:string;
  password:string;
  constructor(private authservice:AuthService, private router: Router, private route:ActivatedRoute) { }

  ngOnInit() {   
  }

  onSubmit(){
    this.authservice.signup(this.email,this.password);
    this.router.navigate(['/welcome']);
  }

}
