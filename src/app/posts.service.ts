import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  myurl="https://jsonplaceholder.typicode.com/posts";

  constructor(public httpClient:HttpClient, private db:AngularFirestore) { }

  getPosts(){
    return this.httpClient.get(this.myurl);
  }

  addPosts(title:string, body:string){
    const post = {title:title, body:body,};
    this.db.collection('posts').add(post);
  }
}
